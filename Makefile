include .env

migrate:
	docker-compose run --rm app python src/manage.py migrate $(if $m, api $m,)

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run --rm app python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

command:
	docker-compose run --rm app python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	docker-compose run --rm app pipenv run isort --check --diff .
	docker-compose run --rm app pipenv run flake8 --config setup.cfg

test:
	docker-compose run --rm app bash -c "cd src/tests && pipenv run pytest"

test_cov:
	docker-compose run --rm app bash -c "cd src/tests && pipenv run pytest --cov=app/internal/"

perfomance_test:
	docker-compose run --rm yandex-tank

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

pull:
	docker pull ${APP_IMAGE}

push:
	docker push ${APP_IMAGE}
