# Backend course

### Переменные окружения

    POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB, POSTGRES_USER, POSTGRES_PASSWORD - данные для подключения к базе данных
    DEBUG - включён ли режим отладки
    SECRET_KEY - секретный ключ для криптографической подписи
    ALLOWED_HOSTS - список строк, представляющих имена хостов/доменов, которые может обслуживать приложение
    TELEGRAM_TOKEN - токен для управления телеграм-ботом
    APP_IMAGE - название докер-образа приложения

## Web-API

    make dev - запуск серверной части приложения

### Эндпоинты

- `me/<telegram_id>` - возвращает информацию о пользователе, имеющем переданный telegram-id, в формате

    {
        "user":
        {
            "username": str,
            "first_name": str,
            "last_name": str,
            "telegram_id": int,
            "phone_number": str,
        }
    }


## Телеграм-бот

    make start_bot - запуск телеграм-бота

### Команды бота

- `/start` – начало диалога с пользователем
- `/set_phone <номер телефона>` – сохраняет указанный номер телефона для пользователя
- `/me` – возвращает информацию о пользователе (заблокирована, пока пользователь не укажет свой номер телефона)

Perfomance tests:
https://overload.yandex.net/593722
https://overload.yandex.net/593734
