from django.contrib import admin

from app.internal.accounts.presentation.admin import AdminAccount
from app.internal.admin_users.presentation.admin import AdminUserAdmin
from app.internal.cards.presentation.admin import AdminCard
from app.internal.issued_tokens.presentation.admin import AdminIssuedToken
from app.internal.transactions.presentation.admin import AdminTransaction
from app.internal.users.presentation.admin import AdminUser

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
