from django.db import models

from app.internal.users.db.models import User


class Account(models.Model):
    id = models.BigAutoField(primary_key=True, serialize=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    number = models.CharField(max_length=20, unique=True, null=False)
