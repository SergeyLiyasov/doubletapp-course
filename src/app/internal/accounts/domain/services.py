import logging
from decimal import Decimal
from typing import List, Optional

from django.db import transaction
from django.db.models import F, Sum
from prometheus_client import Summary

from app.internal.accounts.db.models import Account
from app.internal.transactions.db.models import Transaction
from app.internal.users.db.models import User

logger = logging.getLogger(__name__)
money_transfer_time = Summary("money_transfer_time", "money transfer time")


class AccountsService:
    def get_amount_for_account(self, owner: User, account_number: int) -> Optional[Decimal]:
        return Account.objects.filter(owner=owner, number=account_number).values_list("amount", flat=True).first()

    def get_account(self, account_number: str) -> Optional[Account]:
        account = Account.objects.filter(number=account_number).first()
        return account

    def get_user_account(self, owner: User, account_number: str) -> Optional[Account]:
        account = Account.objects.filter(owner=owner, number=account_number).first()
        return account

    @money_transfer_time.time()
    def try_transfer(self, sender_acc: Account, receiver_acc: Account, sum_to_transfer: float) -> bool:
        if sum_to_transfer < 0 or sender_acc.amount < sum_to_transfer:
            return False
        try:
            with transaction.atomic():
                sender_acc.amount = F("amount") - sum_to_transfer
                receiver_acc.amount = F("amount") + sum_to_transfer
                sender_acc.save(update_fields=("amount",))
                receiver_acc.save(update_fields=("amount",))
                Transaction.objects.create(
                    sender_account=sender_acc,
                    receiver_account=receiver_acc,
                    sum=sum_to_transfer,
                )
        except Exception:
            logger.exception(
                "Не удалось перевести `%s` со счёта `%s` на счёт `%s`",
                sum_to_transfer,
                sender_acc.number,
                receiver_acc.number,
            )
            raise
        else:
            logger.info(
                "Перевод `%s` со счёта `%s` на счёт `%s`", sum_to_transfer, sender_acc.number, receiver_acc.number
            )
        finally:
            sender_acc.refresh_from_db(fields=("amount",))
            receiver_acc.refresh_from_db(fields=("amount",))
        return True

    def get_user_accounts(self, owner: User) -> List[Account]:
        return Account.objects.filter(owner=owner)

    def get_or_create_account(self, owner: User, number: str) -> tuple[Account, bool]:
        return Account.objects.get_or_create(owner=owner, number=number)

    def remove_account(self, owner: User, number: str) -> bool:
        deleted, _ = Account.objects.filter(owner=owner, number=number).delete()
        return deleted > 0

    def get_total_bank_money_amount(self) -> Decimal:
        return Account.objects.all().aggregate(Sum("amount"))["amount__sum"]
