from django.contrib import admin

from app.internal.accounts.db.models import Account


@admin.register(Account)
class AdminAccount(admin.ModelAdmin):
    pass
