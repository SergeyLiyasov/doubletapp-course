from decimal import Decimal

from ninja import Field
from ninja.orm import create_schema

from app.internal.accounts.db.models import Account

AccountSchema = create_schema(
    Account,
    fields=["number"],
)


class AccountOut(AccountSchema):
    amount: Decimal = Field(ge=0)


class AccountIn(AccountSchema):
    ...
