from django.http import HttpRequest, HttpResponseForbidden, HttpResponseNotFound

from app.internal.accounts.domain.services import AccountsService
from app.internal.accounts.presentation.entities import AccountIn, AccountOut
from app.internal.common.presentation.http_responses import HttpResponseConflict, HttpResponseNoContent


class AccountsHandlers:
    def __init__(self, accounts_service: AccountsService):
        self._accounts_service = accounts_service

    def get_all(self, request: HttpRequest) -> list[AccountOut] | HttpResponseForbidden:
        accounts = self._accounts_service.get_user_accounts(request.user)
        return [AccountOut(number=account.number, amount=account.amount) for account in accounts]

    def get_by_number(
        self, request: HttpRequest, number: str
    ) -> AccountOut | HttpResponseNotFound | HttpResponseForbidden:
        amount = self._accounts_service.get_amount_for_account(request.user, number)
        return (
            AccountOut(number=number, amount=amount)
            if amount is not None
            else HttpResponseNotFound("Счёт не существует")
        )

    def create(
        self, request: HttpRequest, account_in: AccountIn
    ) -> AccountOut | HttpResponseConflict | HttpResponseForbidden:
        _, is_new = self._accounts_service.get_or_create_account(request.user, account_in.number)
        return AccountOut(number=account_in.number, amount=0) if is_new else HttpResponseConflict("Счёт уже существует")

    def remove(
        self, request: HttpRequest, number: str
    ) -> HttpResponseNoContent | HttpResponseNotFound | HttpResponseForbidden:
        success = self._accounts_service.remove_account(request.user, number)
        return HttpResponseNoContent() if success else HttpResponseNotFound("Счёт не существует")
