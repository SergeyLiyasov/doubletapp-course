from ninja import NinjaAPI, Router

from app.internal.accounts.presentation.handlers import AccountsHandlers


def add_accounts_router(api: NinjaAPI, accounts_handlers: AccountsHandlers):
    accounts_router = get_accounts_router(accounts_handlers)
    api.add_router("accounts", accounts_router)


def get_accounts_router(accounts_handlers: AccountsHandlers):
    router = Router(tags=["accounts"])

    router.add_api_operation(
        "",
        ["GET"],
        accounts_handlers.get_all,
    )

    router.add_api_operation(
        "",
        ["POST"],
        accounts_handlers.create,
    )

    router.add_api_operation(
        "{number}",
        ["GET"],
        accounts_handlers.get_by_number,
    )

    router.add_api_operation(
        "{number}",
        ["DELETE"],
        accounts_handlers.remove,
    )

    return router
