from ninja import NinjaAPI
from prometheus_client import Gauge

from app.internal.accounts.domain.services import AccountsService
from app.internal.accounts.presentation.handlers import AccountsHandlers
from app.internal.accounts.presentation.routers import add_accounts_router
from app.internal.cards.domain.services import CardsService
from app.internal.cards.presentation.handlers import CardsHandlers
from app.internal.cards.presentation.routers import add_cards_router
from app.internal.issued_tokens.domain.services import IssuedTokensService
from app.internal.middlewares.http_jwt_auth import HttpJwtAuth
from app.internal.transactions.domain.services import TransactionsServices
from app.internal.users.domain.services import UsersService
from app.internal.users.presentation.handlers import UsersHandlers
from app.internal.users.presentation.routers import add_users_router


def get_api() -> NinjaAPI:
    users_service = UsersService()
    issued_tokens_service = IssuedTokensService()
    accounts_service = AccountsService()
    cards_service = CardsService()
    transactions_service = TransactionsServices()

    config_metrics(
        users_service=users_service,
        accounts_service=accounts_service,
        transactions_service=transactions_service,
    )

    users_handlers = UsersHandlers(issued_tokens_service=issued_tokens_service, users_service=users_service)
    accounts_handlers = AccountsHandlers(accounts_service=accounts_service)
    cards_handlers = CardsHandlers(cards_service=cards_service, accounts_service=accounts_service)

    auth = HttpJwtAuth(users_service=users_service)
    api = NinjaAPI(
        title="DT.EDU.BACKEND",
        version="1.0.0",
        auth=[auth],
    )
    add_users_router(api, users_handlers)
    add_accounts_router(api, accounts_handlers)
    add_cards_router(api, cards_handlers)

    return api


def config_metrics(
    accounts_service: AccountsService, transactions_service: TransactionsServices, users_service: UsersService
):
    Gauge("users_count", "bank users count").set_function(users_service.get_users_count)
    Gauge("total_bank_money_amount", "total bank money amount").set_function(
        accounts_service.get_total_bank_money_amount
    )
    Gauge("transferred_money_amount", "transferred money amount").set_function(
        transactions_service.get_transferred_money
    )


ninja_api = get_api()
