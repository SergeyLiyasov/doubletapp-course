from datetime import datetime
from functools import wraps

from prometheus_client import Counter
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Message, Update
from telegram.error import BadRequest
from telegram.ext import CallbackContext

from app.internal.accounts.domain.services import AccountsService
from app.internal.cards.domain.services import CardsService
from app.internal.services.phone_validator import validate_phone_number
from app.internal.transactions.domain.services import TransactionsServices
from app.internal.users.db.models import User
from app.internal.users.domain.services import UsersService

_accounts_service = AccountsService()
_cards_service = CardsService()
_transactions_service = TransactionsServices()
_users_service = UsersService()

transactions_by_username_counter = Counter("transactions_by_username", "transactions by username count")
transactions_by_card_counter = Counter("transactions_by_card", "transactions by card count")
transactions_by_account_counter = Counter("transactions_by_account", "transactions by account count")

_TIME_FORMAT = "%d.%m.%Y"


def _inject_user(func):
    @wraps(func)
    def new_func(update: Update, *args, **kwargs):
        tg_user = update.effective_user
        user = _users_service.update_or_create_user(
            username=tg_user.username,
            first_name=tg_user.first_name,
            last_name=tg_user.last_name,
            telegram_id=tg_user.id,
        )

        return func(update, *args, **kwargs, user=user)

    return new_func


def _require_phone(func):
    @wraps(func)
    def new_func(update: Update, *args, user: User, **kwargs):
        if not user.phone_number:
            update.message.reply_text(_phone_number_not_set_error_message)
            return None
        return func(update, *args, **kwargs, user=user)

    return new_func


@_inject_user
def start(update: Update, context: CallbackContext, *, user: User) -> None:
    update.message.reply_text(_get_greeting_message(user.full_name))


@_inject_user
def set_phone(update: Update, context: CallbackContext, *, user: User) -> None:
    if len(context.args) != 1:
        update.message.reply_text(_invalid_phone_number_message)
        return

    phone = context.args[0]
    if not validate_phone_number(phone):
        update.message.reply_text(_invalid_phone_number_message)
        return

    _users_service.set_phone_for_user(user, phone)
    update.message.reply_text(_phone_number_updated_message)


@_inject_user
@_require_phone
def me(update: Update, context: CallbackContext, *, user: User) -> None:
    update.message.reply_text(_get_user_info_message(user))


@_inject_user
@_require_phone
def get_amounts(update: Update, context: CallbackContext, *, user: User) -> None:
    if len(context.args) != 1:
        update.message.reply_text(_invalid_item_type_error_message)
        return

    item_type = context.args[0]
    match item_type:
        case "card":
            items = _cards_service.get_user_cards(user)
        case "account":
            items = _accounts_service.get_user_accounts(user)
        case _:
            update.message.reply_text(_invalid_item_type_error_message)
            return

    if not len(items):
        update.message.reply_text(_get_no_items_error_message(item_type))
        return
    keyboard = [
        [
            InlineKeyboardButton(str(item.number), callback_data=f"get_amount {item_type} {item.number}")
            for item in items
        ]
    ]
    update.message.reply_text(_get_items_list_message(item_type), reply_markup=InlineKeyboardMarkup(keyboard))


@_inject_user
@_require_phone
def get_amount_callback(update: Update, context: CallbackContext, *, user: User) -> None:
    query = update.callback_query
    command, item_type, number = query.data.split(maxsplit=2)
    match item_type:
        case "card":
            amount = _cards_service.get_amount_for_card(user, number)
        case "account":
            amount = _accounts_service.get_amount_for_account(user, number)
        case _:
            raise KeyError
    message_text = str(amount) if amount is not None else _get_item_not_exist_error_message(item_type)
    _try_edit_message_text(query.message, message_text)


@_inject_user
@_require_phone
def get_contacts(update: Update, context: CallbackContext, *, user: User):
    contacts = [e["username"] for e in user.contacts.values("username").all()]
    update.message.reply_text(_get_contacts_list_message(contacts))


@_inject_user
@_require_phone
def add_contacts(update: Update, context: CallbackContext, *, user: User):
    if not context.args or len(context.args) < 1:
        update.message.reply_text(_wrong_args_number_error_message)
        return

    _users_service.add_to_contacts(user, context.args)
    update.message.reply_text(_get_contacts_added_message(context.args))


@_inject_user
@_require_phone
def remove_contacts(update: Update, context: CallbackContext, *, user: User):
    if not context.args or len(context.args) < 1:
        update.message.reply_text(_wrong_args_number_error_message)
        return

    _users_service.delete_from_contacts(user, context.args)
    update.message.reply_text(_get_contacts_deleted_message(context.args))


@_inject_user
@_require_phone
def transfer_money(update: Update, context: CallbackContext, *, user: User):
    if not context.args or len(context.args) != 4:
        update.message.reply_text(_wrong_args_number_error_message)
        return

    amount, source, dest_type, destination = context.args
    amount = int(amount)

    if amount <= 0:
        update.message.reply_text(_below_zero_error_message)
        return

    match dest_type:
        case "user":
            receiver_user = _users_service.get_user(destination)
            accounts = receiver_user and _accounts_service.get_user_accounts(receiver_user)
            if not accounts:
                update.message.reply_text(_no_acc_error_message)
                return
            dest_account = accounts[0]
            counter = transactions_by_username_counter
        case "card":
            card = _cards_service.get_card(destination)
            if not card:
                update.message.reply_text(_no_acc_error_message)
                return
            dest_account = card.account
            counter = transactions_by_card_counter
        case "account":
            dest_account = _accounts_service.get_account(destination)
            counter = transactions_by_account_counter
        case _:
            update.message.reply_text(_invalid_receiver_error_message)
            return

    source_account = _accounts_service.get_account(source)
    if source_account.owner.telegram_id != user.telegram_id:
        update.message.reply_text(_not_own_account_error_message)
        return

    _accounts_service.try_transfer(sender_acc=source_account, receiver_acc=dest_account, sum_to_transfer=amount)
    counter.inc()
    update.message.reply_text(_get_transfered_message(source_account.amount))


@_inject_user
@_require_phone
def get_transactions(update: Update, context: CallbackContext, *, user: User):
    if not context.args or not 2 <= len(context.args) <= 4:
        update.message.reply_text(_invalid_transactions_info_args_error_message)
        return

    source_type = context.args[0]
    source = context.args[1]
    from_, from_parsed = _try_parse_time(context.args[2]) if len(context.args) >= 3 else (None, True)
    to, to_parsed = _try_parse_time(context.args[3]) if len(context.args) >= 4 else (None, True)

    if not from_parsed or not to_parsed:
        update.message.reply_text(_invalid_time_format_error_message)
        return

    match source_type:
        case "card":
            card = _cards_service.get_card(source)
            if not card:
                update.message.reply_text(_not_own_account_error_message)
                return
            source_account = card.account
        case "account":
            source_account = _accounts_service.get_account(source)
            if not source_account:
                update.message.reply_text(_not_own_account_error_message)
                return
        case _:
            update.message.reply_text(_invalid_transactions_info_args_error_message)
            return

    if source_account.owner.telegram_id != user.telegram_id:
        update.message.reply_text(_not_own_account_error_message)
        return

    transactions = _transactions_service.get_transactions_from(source_account, from_, to)

    update.message.reply_text(_get_transactions_message(transactions))


@_inject_user
@_require_phone
def get_users_interacted_with(update: Update, context: CallbackContext, *, user: User):
    update.message.reply_text(_get_interacted_with_message(_transactions_service.get_usernames_interacted_with(user)))


@_inject_user
@_require_phone
def set_password(update: Update, context: CallbackContext, *, user: User):
    if not context.args or len(context.args) != 1:
        update.message.reply_text(_wrong_args_number_error_message)
        return

    _users_service.save_password(user, context.args[0])
    update.message.reply_text(_password_changed_message)


def _try_edit_message_text(message: Message, text: str):
    try:
        message.edit_text(text, reply_markup=message.reply_markup)
    except BadRequest as e:
        if not e.message.startswith("Message is not modified"):
            raise


def _get_greeting_message(name: str):
    return f"Привет, {name}!"


def _get_user_info_message(user: User):
    return (
        f"Данные о пользователе:\n"
        f"id: {user.telegram_id}\n"
        f"username: {user.username}\n"
        f"имя: {user.full_name}\n"
        f"номер телефона: {user.phone_number}\n"
    )


def _get_no_items_error_message(item_type):
    match item_type:
        case "account":
            return "У вас ещё нет открытых счетов"
        case "card":
            return "У вас ещё нет открытых карт"
    return None


def _get_items_list_message(item_type):
    match item_type:
        case "account":
            return "Список ваших счетов:"
        case "card":
            return "Список ваших карт:"
    return None


def _get_item_not_exist_error_message(item_type):
    match item_type:
        case "account":
            return "Этот счёт больше не существует"
        case "card":
            return "Эта карта больше не существует"
    return None


def _get_contacts_list_message(contacts):
    if not contacts:
        return "Ваш список контактов пуст. Наполните его с помощью команды /add_contact"
    return "Список контактов:\n" + ",\n".join(contacts)


def _get_contacts_added_message(contacts_names):
    contacts = "`\n`".join(contacts_names)
    return f"Контакты:\n`{contacts}`\nдобавлены в список контактов"


def _get_contacts_deleted_message(contacts_names):
    contacts = "`\n`".join(contacts_names)
    return f"Контакты:\n`{contacts}`\nудалены из списка контактов"


def _get_transfered_message(amount):
    return f"Успешно! Состояние вашего счёта: {amount}"


def _get_transactions_message(transactions):
    return "Время\tСчёт-получатель\tСумма\n" + "\n".join(
        "\t".join((t["datetime"].strftime(_TIME_FORMAT), str(t["receiver_account__number"]), str(t["sum"])))
        for t in transactions
    )


def _get_interacted_with_message(usernames):
    return "\n".join(usernames) or "Ещё нет ни одного перевода"


def _try_parse_time(string):
    try:
        return datetime.strptime(string, _TIME_FORMAT), True
    except:
        return None, False


_invalid_transactions_info_args_error_message = (
    "Неверный формат аргументов, ожидался формат /get_transactions card|account <номер> <дата начала> <дата конца>"
)
_invalid_time_format_error_message = "Неверный формат времени, ожидался формат 13.01.2000"
_password_changed_message = "Успешно! Пароль сменён"
_wrong_args_number_error_message = "Неверное количество аргументов"
_below_zero_error_message = "Невозможно перевести неположительную сумму"
_not_own_account_error_message = "Невозможно совершить перевод: вы не являетесь владельцем счёта-отправителя"
_invalid_receiver_error_message = "Неверный тип получателя"
_no_acc_error_message = "Невозможно совершить перевод: счёт не существует"
_invalid_phone_number_message = "Неверный формат номера телефона, ожидался формат /set_phone +70001112233"
_phone_number_updated_message = "Номер телефона обновлён"
_phone_number_not_set_error_message = (
    "Нет доступа к команде. Сперва требуется указать номер телефона (/set_phone <номер телефона>)"
)
_invalid_item_type_error_message = (
    "Неверный формат аргумента, ожидался формат /get_amounts card или /get_amounts account"
)
