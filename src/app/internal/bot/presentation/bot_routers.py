import traceback
from functools import wraps

from telegram import Bot, Update
from telegram.ext import CallbackQueryHandler, CommandHandler, Dispatcher

from app.internal.bot.presentation.bot_handlers import (
    add_contacts,
    get_amount_callback,
    get_amounts,
    get_contacts,
    get_transactions,
    get_users_interacted_with,
    me,
    remove_contacts,
    set_password,
    set_phone,
    start,
    transfer_money,
)
from config.settings import DEBUG, TELEGRAM_TOKEN


class AppBot:
    def __init__(self):
        self.bot = Bot(TELEGRAM_TOKEN)
        self.dispatcher = Dispatcher(self.bot, update_queue=None, workers=1, use_context=True)
        self.register_handlers()

    def process_update(self, update_json):
        update = Update.de_json(update_json, self.bot)
        self.dispatcher.process_update(update)

    def register_handlers(self):
        self.dispatcher.add_handler(CommandHandler("start", AppBot._wrap_handler(start)))
        self.dispatcher.add_handler(CommandHandler("set_phone", AppBot._wrap_handler(set_phone)))
        self.dispatcher.add_handler(CommandHandler("me", AppBot._wrap_handler(me)))
        self.dispatcher.add_handler(CommandHandler("get_amounts", AppBot._wrap_handler(get_amounts)))
        self.dispatcher.add_handler(CommandHandler("get_contacts", AppBot._wrap_handler(get_contacts)))
        self.dispatcher.add_handler(CommandHandler("add_contact", AppBot._wrap_handler(add_contacts)))
        self.dispatcher.add_handler(CommandHandler("remove_contact", AppBot._wrap_handler(remove_contacts)))
        self.dispatcher.add_handler(CommandHandler("transfer", AppBot._wrap_handler(transfer_money)))
        self.dispatcher.add_handler(CommandHandler("get_transactions", AppBot._wrap_handler(get_transactions)))
        self.dispatcher.add_handler(CommandHandler("get_interactions", AppBot._wrap_handler(get_users_interacted_with)))
        self.dispatcher.add_handler(CommandHandler("set_password", AppBot._wrap_handler(set_password)))
        self.dispatcher.add_handler(
            CallbackQueryHandler(AppBot._wrap_handler(get_amount_callback), pattern="get_amount")
        )

    @staticmethod
    def _wrap_handler(func):
        if not DEBUG:
            return func

        @wraps(func)
        def new_func(update: Update, *args, **kwargs):
            try:
                func(update, *args, **kwargs)
            except Exception as e:
                update.effective_message.reply_text("🔥 Error:\n" + repr(e) + "\n\n" + traceback.format_exc())
                raise

        return new_func


bot = AppBot()
