from django.db import models

from app.internal.accounts.db.models import Account
from app.internal.users.db.models import User


class Card(models.Model):
    id = models.BigAutoField(primary_key=True, serialize=False)
    number = models.CharField(max_length=16, unique=True, null=False)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
