from decimal import Decimal
from typing import List, Optional

from app.internal.accounts.db.models import Account
from app.internal.cards.db.models import Card
from app.internal.users.db.models import User


class CardsService:
    def get_amount_for_card(self, owner: User, card_number: int) -> Optional[Decimal]:
        return (
            Card.objects.filter(account__owner=owner, number=card_number)
            .values_list("account__amount", flat=True)
            .first()
        )

    def get_card(self, card_number: str) -> Optional[Card]:
        card = Card.objects.filter(number=card_number).first()
        return card

    def get_user_card(self, owner: User, card_number: str) -> Optional[Card]:
        card = Card.objects.filter(account__owner=owner, number=card_number).first()
        return card

    def get_user_cards(self, owner: User) -> List[Card]:
        return Card.objects.filter(account__owner=owner)

    def get_or_create_card(self, owner: User, number: str, account_number: str) -> tuple[Card, bool]:
        def lazy_account():
            return Account.objects.filter(owner=owner, number=account_number).first()

        return Card.objects.get_or_create(number=number, defaults=dict(account=lazy_account))

    def remove_card(self, owner: User, number: str) -> bool:
        deleted, _ = Card.objects.filter(account__owner=owner, number=number).delete()
        return deleted > 0

    def change_account_for_card(self, card: Card, new_account: Account):
        card.account = new_account
        card.save(update_fields=("account",))
