from ninja import Schema
from ninja.orm import create_schema

from app.internal.cards.db.models import Card

CardSchema = create_schema(
    Card,
    fields=["number"],
)


class CardOut(CardSchema):
    account_number: str


class CardIn(CardSchema):
    account_number: str


class CardChangeAccountIn(Schema):
    account_number: str
