from django.http import HttpRequest, HttpResponseForbidden, HttpResponseNotFound

from app.internal.accounts.domain.services import AccountsService
from app.internal.cards.domain.services import CardsService
from app.internal.cards.presentation.entities import CardChangeAccountIn, CardIn, CardOut
from app.internal.common.presentation.http_responses import HttpResponseConflict, HttpResponseNoContent


class CardsHandlers:
    def __init__(self, cards_service: CardsService, accounts_service: AccountsService):
        self._cards_service = cards_service
        self._accounts_service = accounts_service

    def get_all(self, request: HttpRequest) -> list[CardOut] | HttpResponseForbidden:
        cards = self._cards_service.get_user_cards(request.user)
        return [CardOut(number=card.number, account_number=card.account.number) for card in cards]

    def get_by_number(
        self, request: HttpRequest, number: str
    ) -> CardOut | HttpResponseNotFound | HttpResponseForbidden:
        card = self._cards_service.get_user_card(request.user, number)
        return (
            CardOut(number=number, account_number=card.account.number)
            if card is not None
            else HttpResponseNotFound("Карта не существует")
        )

    def create(self, request: HttpRequest, card_in: CardIn) -> CardOut | HttpResponseConflict | HttpResponseForbidden:
        _, is_new = self._cards_service.get_or_create_card(request.user, card_in.number, card_in.account_number)
        return (
            CardOut(number=card_in.number, account_number=card_in.account_number)
            if is_new
            else HttpResponseConflict("Карта уже существует")
        )

    def remove(
        self, request: HttpRequest, number: str
    ) -> HttpResponseNoContent | HttpResponseNotFound | HttpResponseForbidden:
        success = self._cards_service.remove_card(request.user, number)
        return HttpResponseNoContent() if success else HttpResponseNotFound("Карта не существует")

    def change_account(
        self, request: HttpRequest, number: str, card_in: CardChangeAccountIn
    ) -> HttpResponseNoContent | HttpResponseNotFound | HttpResponseForbidden:
        card = self._cards_service.get_user_card(request.user, number)
        if card is None:
            return HttpResponseNotFound("Карта не существует")
        account = self._accounts_service.get_user_account(request.user, card_in.account_number)
        if account is None:
            return HttpResponseNotFound("Счёт не существует")
        self._cards_service.change_account_for_card(card, account)
        return HttpResponseNoContent()
