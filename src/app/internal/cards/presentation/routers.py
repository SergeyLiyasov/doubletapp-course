from ninja import NinjaAPI, Router

from app.internal.cards.presentation.handlers import CardsHandlers


def add_cards_router(api: NinjaAPI, cards_handlers: CardsHandlers):
    cards_router = get_cards_router(cards_handlers)
    api.add_router("cards", cards_router)


def get_cards_router(cards_handlers: CardsHandlers):
    router = Router(tags=["cards"])

    router.add_api_operation(
        "",
        ["GET"],
        cards_handlers.get_all,
    )

    router.add_api_operation(
        "",
        ["POST"],
        cards_handlers.create,
    )

    router.add_api_operation(
        "{number}",
        ["GET"],
        cards_handlers.get_by_number,
    )

    router.add_api_operation(
        "{number}",
        ["DELETE"],
        cards_handlers.remove,
    )

    router.add_api_operation(
        "{number}",
        ["PUT"],
        cards_handlers.change_account,
    )

    return router
