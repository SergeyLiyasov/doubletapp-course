import enum
from datetime import datetime, timedelta
from typing import Optional
from uuid import UUID

import jwt

from app.internal.issued_tokens.db.models import IssuedToken
from app.internal.users.db.models import User
from config.settings import ACCESS_TOKEN_TTL, JWT_SECRET, REFRESH_TOKEN_TTL


class TokenValidationResult(enum.Enum):
    Success = 0
    InvalidSignature = 1
    Expired = 2


class IssuedTokensService:
    def generate_jwt_token(self, user: User, ttl: int, jti: UUID = None) -> str:
        raw = {"id": user.id, "exp": datetime.now().timestamp() + ttl}
        if jti is not None:
            raw["jti"] = str(jti)
        return jwt.encode(raw, JWT_SECRET, algorithm="HS256")

    def decode_and_verify_jwt_token(self, token: str) -> tuple[Optional[dict[str, str]], TokenValidationResult]:
        try:
            return (
                jwt.decode(token, JWT_SECRET, algorithms=("HS256",), options={"verify_signature": True}),
                TokenValidationResult.Success,
            )
        except jwt.ExpiredSignatureError:
            return (
                jwt.decode(token, JWT_SECRET, algorithms=("HS256",), options={"verify_signature": False}),
                TokenValidationResult.Expired,
            )
        except jwt.InvalidSignatureError:
            return None, TokenValidationResult.InvalidSignature

    def generate_and_save_refresh_token(self, user: User, ttl: int) -> str:
        token_db = IssuedToken.objects.create(user=user)
        return self.generate_jwt_token(user, ttl, token_db.jti)

    def generate_access_refresh_tokens(self, user: User) -> tuple[str, str]:
        refresh_token = self.generate_and_save_refresh_token(user, REFRESH_TOKEN_TTL)
        access_token = self.generate_jwt_token(user, ACCESS_TOKEN_TTL)
        return access_token, refresh_token

    def get_token_model(self, jti: UUID) -> Optional[IssuedToken]:
        return IssuedToken.objects.filter(jti=jti).select_related("user").first()

    def revoke_all_tokens(self, user: User):
        IssuedToken.objects.filter(
            user=user, created_at__gt=datetime.now() - timedelta(seconds=REFRESH_TOKEN_TTL)
        ).delete()
        IssuedToken.objects.filter(user=user).update(revoked=True)

    def revoke_token(self, issued_token: IssuedToken):
        issued_token.revoked = True
        issued_token.save(update_fields=("revoked",))
