from django.contrib import admin

from app.internal.issued_tokens.db.models import IssuedToken


@admin.register(IssuedToken)
class AdminIssuedToken(admin.ModelAdmin):
    pass
