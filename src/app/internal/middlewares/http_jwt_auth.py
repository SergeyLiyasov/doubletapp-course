import jwt
from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.users.domain.services import UsersService
from config.settings import JWT_SECRET


class HttpJwtAuth(HttpBearer):
    def __init__(self, users_service: UsersService):
        super().__init__()
        self._users_service = users_service

    def authenticate(self, request: HttpRequest, token: str):
        try:
            payload = jwt.decode(token, JWT_SECRET, algorithms=("HS256",), verify=True)
            user = self._users_service.get_user_by_id(payload["id"])
            request.user = user
        except:
            return None

        return token
