import re

PHONE_RE = re.compile(r"^\+?\d{9,15}$")


def validate_phone_number(phone_number: str) -> bool:
    return PHONE_RE.fullmatch(phone_number) is not None
