from django.db import models
from django_prometheus.models import ExportModelOperationsMixin

from app.internal.accounts.db.models import Account


class Transaction(ExportModelOperationsMixin("transaction"), models.Model):
    sender_account = models.ForeignKey(Account, on_delete=models.PROTECT, related_name="sender_account")
    receiver_account = models.ForeignKey(Account, on_delete=models.PROTECT, related_name="receiver_account")
    sum = models.DecimalField(max_digits=10, decimal_places=2)
    datetime = models.DateTimeField(auto_now_add=True)
