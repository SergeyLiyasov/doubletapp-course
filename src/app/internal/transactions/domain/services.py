from datetime import datetime
from decimal import Decimal
from typing import Iterable

from django.db.models import Sum

from app.internal.accounts.db.models import Account
from app.internal.transactions.db.models import Transaction
from app.internal.users.db.models import User


class TransactionsServices:
    def get_transactions_from(
        self, acc_sender: Account, after: datetime = None, before: datetime = None
    ) -> Iterable[dict]:
        after = after or datetime.min
        before = before or datetime.max
        return Transaction.objects.filter(
            sender_account=acc_sender,
            datetime__gte=after,
            datetime__lte=before,
        ).values("datetime", "receiver_account__number", "sum")

    def get_usernames_interacted_with(self, user: User) -> Iterable[str]:
        receivers = (
            Transaction.objects.filter(sender_account__owner=user)
            .exclude(receiver_account__owner__username=user.username)
            .values("receiver_account__owner__username")
        )
        senders = (
            Transaction.objects.filter(receiver_account__owner=user)
            .exclude(sender_account__owner__username=user.username)
            .values("sender_account__owner__username")
        )
        return (
            receivers.union(senders)
            .order_by("sender_account__owner__username")
            .values_list("sender_account__owner__username", flat=True)
            .all()
        )

    def get_transferred_money(self) -> Decimal:
        return Transaction.objects.aggregate(Sum("sum"))["sum__sum"]
