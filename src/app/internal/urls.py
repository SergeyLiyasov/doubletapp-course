from django.urls import include, path

from app.internal.app import ninja_api

urlpatterns = [
    path("", ninja_api.urls),
    path("", include("django_prometheus.urls")),
]
