from django.core.validators import RegexValidator
from django.db import models

from app.internal.services.phone_validator import PHONE_RE


class User(models.Model):
    telegram_id = models.IntegerField(unique=True, db_index=True)
    username = models.CharField(max_length=250, null=True)
    first_name = models.CharField(max_length=250, null=True, blank=True)
    last_name = models.CharField(max_length=250, null=True, blank=True)
    phone_validator = RegexValidator(regex=PHONE_RE, message="Phone number must have format: +70001112233.")
    phone_number = models.CharField(validators=[phone_validator], max_length=20, blank=True)
    contacts = models.ManyToManyField("self", blank=True, symmetrical=False)
    password_hash = models.CharField(max_length=255, null=False, editable=False)

    def __str__(self):
        return f"User: {self.username}"

    @property
    def full_name(self) -> str:
        if self.first_name is None:
            if self.last_name is None:
                return "<unknown>"
            return self.last_name

        if self.last_name is None:
            return self.first_name

        return f"{self.first_name} {self.last_name}"

    class Meta:
        db_table = "user"
        constraints = [models.UniqueConstraint(fields=["username"], name="unique_username")]
