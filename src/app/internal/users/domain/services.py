import hashlib
from typing import Optional

from app.internal.users.db.models import User
from config.settings import SALT


class UsersService:
    def hash_password(self, password: str) -> str:
        return hashlib.sha256((password + SALT).encode()).hexdigest()

    def check_password(self, user: User, password: str) -> bool:
        return user.password_hash == self.hash_password(password)

    def save_password(self, user: User, password: str):
        user.password_hash = self.hash_password(password)
        user.save(update_fields=("password_hash",))

    def get_or_create_user(self, username: str, first_name: str, last_name: str, telegram_id: int) -> User:
        user, _ = User.objects.get_or_create(
            telegram_id=telegram_id,
            defaults=dict(
                username=username,
                first_name=first_name,
                last_name=last_name,
            ),
        )
        return user

    def update_or_create_user(self, username: str, first_name: str, last_name: str, telegram_id: int) -> User:
        user, _ = User.objects.update_or_create(
            telegram_id=telegram_id,
            defaults=dict(
                username=username,
                first_name=first_name,
                last_name=last_name,
            ),
        )
        return user

    def get_user_by_id(self, id: int) -> Optional[User]:
        return User.objects.filter(id=id).first()

    def get_user(self, telegram_id: int) -> Optional[User]:
        return User.objects.filter(telegram_id=telegram_id).first()

    def get_user_by_username(self, username: str) -> User:
        return User.objects.filter(username=username).first()

    def set_phone_for_user(self, user: User, phone_number: str) -> None:
        user.phone_number = phone_number
        user.save(update_fields=["phone_number"])

    def add_to_contacts(self, user: User, contacts_names: list[str]):
        user.contacts.add(*User.objects.filter(username__in=contacts_names).values_list("id", flat=True))

    def delete_from_contacts(self, user: User, contacts_names: list[str]):
        user.contacts.remove(*User.objects.filter(username__in=contacts_names).values_list("id", flat=True))

    def serialize_user(self, user: User) -> dict[str, str]:
        return {
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "telegram_id": str(user.telegram_id),
            "phone_number": user.phone_number,
        }

    def get_users_count(self) -> int:
        return User.objects.count()
