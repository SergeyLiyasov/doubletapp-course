from ninja import Schema
from ninja.orm import create_schema

from app.internal.users.db.models import User

UserSchema = create_schema(
    User,
    fields=[
        "telegram_id",
        "username",
        "first_name",
        "last_name",
        "phone_number",
    ],
)


class UserOut(UserSchema):
    ...


class CredentialsSchema(Schema):
    username: str
    password: str
