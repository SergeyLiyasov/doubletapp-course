from uuid import UUID

from django.http import HttpRequest, HttpResponseBadRequest, HttpResponseForbidden, HttpResponseNotFound, JsonResponse
from ninja import NinjaAPI

from app.internal.issued_tokens.domain.services import IssuedTokensService, TokenValidationResult
from app.internal.users.domain.services import UsersService
from app.internal.users.presentation.entities import CredentialsSchema


class UsersHandlers:
    def __init__(self, issued_tokens_service: IssuedTokensService, users_service: UsersService):
        self._issued_tokens_service = issued_tokens_service
        self._users_service = users_service

    def get_user_info(self, request: HttpRequest) -> JsonResponse | HttpResponseNotFound | HttpResponseForbidden:
        db_user = request.user
        if db_user is None:
            return HttpResponseNotFound("Пользователь ещё не пользовался телеграм-ботом")
        if not db_user.phone_number:
            return HttpResponseForbidden("Пользователь не указал свой номер телефона")

        return JsonResponse(data={"user": self._users_service.serialize_user(db_user)})

    def authorize_user(
        self, request: HttpRequest, credentials: CredentialsSchema
    ) -> JsonResponse | HttpResponseNotFound:
        db_user = self._users_service.get_user_by_username(credentials.username)

        if not db_user or not self._users_service.check_password(db_user, credentials.password):
            return HttpResponseForbidden("Неправильный логин или пароль")

        access_token, refresh_token = self._issued_tokens_service.generate_access_refresh_tokens(db_user)
        return JsonResponse({"access_token": access_token, "refresh_token": refresh_token})

    def refresh_token(self, request: HttpRequest, refresh_token: str) -> JsonResponse | HttpResponseNotFound:
        decoded_token, validation_result = self._issued_tokens_service.decode_and_verify_jwt_token(refresh_token)

        if validation_result is TokenValidationResult.InvalidSignature:
            return HttpResponseBadRequest("refresh token has invalid signature")

        if "jti" not in decoded_token:
            return HttpResponseBadRequest("expected refresh token")

        token_model = self._issued_tokens_service.get_token_model(UUID(decoded_token["jti"]))

        user = token_model.user if token_model is not None else self._users_service.get_user_by_id(decoded_token["id"])

        if validation_result is TokenValidationResult.Expired or token_model is None or token_model.revoked:
            self._issued_tokens_service.revoke_all_tokens(user)
            return HttpResponseBadRequest("refresh token has expired or was revoked")

        if validation_result is not TokenValidationResult.Success:
            raise KeyError(f"Unexpected result of token validation: {validation_result}")

        self._issued_tokens_service.revoke_token(token_model)
        new_access_token, new_refresh_token = self._issued_tokens_service.generate_access_refresh_tokens(
            token_model.user
        )

        return JsonResponse({"access_token": new_access_token, "refresh_token": new_refresh_token})
