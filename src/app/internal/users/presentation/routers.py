from typing import List

from ninja import NinjaAPI, Router

from app.internal.middlewares.http_jwt_auth import HttpJwtAuth
from app.internal.users.presentation.handlers import UsersHandlers

from .entities import UserOut


def add_users_router(api: NinjaAPI, user_handlers: UsersHandlers):
    users_router = get_users_router(user_handlers)
    api.add_router("users", users_router)


def get_users_router(users_handlers: UsersHandlers):
    router = Router(tags=["users"])

    router.add_api_operation(
        "me",
        ["GET"],
        users_handlers.get_user_info,
    )

    router.add_api_operation(
        "login",
        ["POST"],
        users_handlers.authorize_user,
        auth=None,
    )

    router.add_api_operation(
        "refresh_token",
        ["GET"],
        users_handlers.refresh_token,
        auth=None,
    )

    return router
