# Generated by Django 4.2 on 2023-04-23 20:29

import uuid

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0004_remove_card_owner_user_contacts"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="password_hash",
            field=models.CharField(default="", editable=False, max_length=255),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name="IssuedToken",
            fields=[
                ("jti", models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("revoked", models.BooleanField(default=False)),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, related_name="refresh_tokens", to="app.user"
                    ),
                ),
            ],
        ),
    ]
