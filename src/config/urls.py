from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt

from app.internal.bot.presentation.handlers import BotView
from config.settings import TELEGRAM_TOKEN

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("app.internal.urls")),
    path(f"{TELEGRAM_TOKEN}/", csrf_exempt(BotView.as_view())),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
