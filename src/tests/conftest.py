import pytest
from django.test import Client
from utils import create_account

from app.internal.users.db.models import User


@pytest.fixture(scope="function")
@pytest.mark.django_db
def test_user(telegram_username="first", first_name="first"):
    return User.objects.create(
        username=telegram_username,
        first_name=first_name,
        telegram_id=1,
        phone_number="",
    )


@pytest.fixture(scope="function")
@pytest.mark.django_db
def other_test_user(telegram_username="second", first_name="second"):
    return User.objects.create(
        username=telegram_username,
        first_name=first_name,
        telegram_id=2,
        phone_number="",
    )


@pytest.fixture(scope="function")
def test_client():
    return Client()


@pytest.fixture(scope="function")
def test_account():
    return create_account("12233344445555599999", "some_one", "some_one")


@pytest.fixture(scope="function")
def test_account_second():
    return create_account("12233344445555500000", "some_two", "some_two")
