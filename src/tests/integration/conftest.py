from unittest import mock

import pytest
from telegram import Update

from app.internal.users.db.models import User


@pytest.fixture(scope="function")
def effective_chat_mock(test_user: User):
    mocked_effective_chat = mock.MagicMock(
        id=test_user.telegram_id,
        username=test_user.username,
        first_name=test_user.first_name,
        last_name=test_user.last_name,
    )
    return mocked_effective_chat


@pytest.fixture(scope="function")
def update_mock(effective_chat_mock):
    mocked_update = mock.MagicMock(effective_chat=effective_chat_mock, effective_user=effective_chat_mock)
    return mocked_update


@pytest.fixture(scope="function")
def context_mock(update_mock: Update):
    context_mock = mock.MagicMock(args=update_mock.message.text.split()[1:])
    return context_mock


@pytest.fixture(scope="function")
def update_mock_set_phone(effective_chat_mock):
    mocked_msg = mock.MagicMock(text="/set_phone +79641952255")
    mocked_update = mock.MagicMock(
        effective_chat=effective_chat_mock, effective_user=effective_chat_mock, message=mocked_msg
    )
    return mocked_update


@pytest.fixture(scope="function")
def set_phone_context_mock(update_mock_set_phone: Update):
    context_mock = mock.MagicMock(args=update_mock_set_phone.message.text.split()[1:])
    return context_mock


@pytest.fixture(scope="function")
def adding_update_mock():
    mocked_msg = mock.MagicMock(text="/add_contact second")
    effective_chat_mock = mock.MagicMock(id="1", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(
        effective_chat=effective_chat_mock, effective_user=effective_chat_mock, message=mocked_msg
    )
    return mocked_update


@pytest.fixture(scope="function")
def adding_context_mock(adding_update_mock: Update):
    context_mock = mock.MagicMock(args=adding_update_mock.message.text.split()[1:])
    return context_mock


@pytest.fixture(scope="function")
def removing_update_mock():
    mocked_msg = mock.MagicMock(text="/remove_contact second")
    effective_chat_mock = mock.MagicMock(id="1", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(
        effective_chat=effective_chat_mock, effective_user=effective_chat_mock, message=mocked_msg
    )
    return mocked_update


@pytest.fixture(scope="function")
def removing_context_mock(removing_update_mock: Update):
    context_mock = mock.MagicMock(args=removing_update_mock.message.text.split()[1:])
    return context_mock


@pytest.fixture(scope="function")
def get_amounts_update_mock():
    mocked_msg = mock.MagicMock(text="/get_amounts account")
    effective_chat_mock = mock.MagicMock(id="1", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(
        effective_chat=effective_chat_mock, effective_user=effective_chat_mock, message=mocked_msg
    )
    return mocked_update


@pytest.fixture(scope="function")
def get_amounts_context_mock(get_amounts_update_mock: Update):
    context_mock = mock.MagicMock(args=get_amounts_update_mock.message.text.split()[1:])
    return context_mock


@pytest.fixture(scope="function")
def get_account_amount_update_mock():
    effective_chat_mock = mock.MagicMock(id="1", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(effective_chat=effective_chat_mock, effective_user=effective_chat_mock)
    mocked_update.callback_query.data = "get_amount account 1"
    return mocked_update
