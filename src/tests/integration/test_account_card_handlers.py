import pytest
from telegram import InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext

from app.internal.accounts.db.models import Account
from app.internal.bot.presentation.bot_handlers import get_amount_callback, get_amounts, set_phone
from app.internal.users.db.models import User


@pytest.mark.integration
@pytest.mark.django_db
def test_get_amounts_when_none(update_mock: Update, context_mock: CallbackContext):
    set_phone(update_mock, context_mock)
    user = User.objects.filter(username=update_mock.effective_chat.username).first()
    assert user
    get_amounts(update_mock, context_mock)
    reply_markup = update_mock.message.reply_text.call_args.kwargs.get("reply_markup")
    assert reply_markup is None


@pytest.mark.integration
@pytest.mark.django_db
def test_get_amounts_when_single(
    update_mock_set_phone: Update,
    set_phone_context_mock: CallbackContext,
    get_amounts_update_mock: Update,
    get_amounts_context_mock: CallbackContext,
):
    set_phone(update_mock_set_phone, set_phone_context_mock)
    user = User.objects.filter(username=get_amounts_update_mock.effective_chat.username).first()
    assert user
    account = Account.objects.create(id=1, owner=user, amount=20, number="1")
    get_amounts(get_amounts_update_mock, get_amounts_context_mock)
    reply_markup = get_amounts_update_mock.message.reply_text.call_args.kwargs["reply_markup"]
    assert isinstance(reply_markup, InlineKeyboardMarkup)
    buttons = [button for row in reply_markup.inline_keyboard for button in row]
    assert len(buttons) == 1
    assert account.number in buttons[0].text


@pytest.mark.integration
@pytest.mark.django_db
def test_get_amount_callback(
    update_mock_set_phone: Update,
    set_phone_context_mock: CallbackContext,
    get_account_amount_update_mock: Update,
    context_mock: CallbackContext,
):
    set_phone(update_mock_set_phone, set_phone_context_mock)
    user = User.objects.filter(username=get_account_amount_update_mock.effective_chat.username).first()
    assert user
    account = Account.objects.create(id=1, owner=user, amount=20, number="1")
    get_amount_callback(get_account_amount_update_mock, context_mock)
    get_account_amount_update_mock.callback_query.message.edit_text.assert_called_once()
    text = get_account_amount_update_mock.callback_query.message.edit_text.call_args.args[0]
    assert str(account.amount) in text
