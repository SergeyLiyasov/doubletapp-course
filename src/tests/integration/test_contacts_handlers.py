import pytest
from telegram import Update, User
from telegram.ext import CallbackContext

from app.internal.bot.presentation.bot_handlers import add_contacts, remove_contacts


@pytest.mark.integration
@pytest.mark.django_db
def test_add(test_user: User, other_test_user: User, adding_update_mock: Update, adding_context_mock: CallbackContext):
    add_contacts(adding_update_mock, adding_context_mock)
    adding_update_mock.message.reply_text.assert_called_once()


@pytest.mark.integration
@pytest.mark.django_db
def test_remove(
    test_user: User,
    other_test_user: User,
    adding_update_mock: Update,
    removing_update_mock: Update,
    removing_context_mock: CallbackContext,
):
    add_contacts(adding_update_mock, removing_context_mock)
    remove_contacts(removing_update_mock, removing_context_mock)
    removing_update_mock.message.reply_text.assert_called()
