from unittest.mock import ANY

import pytest
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bot.presentation.bot_handlers import (
    _phone_number_not_set_error_message,
    add_contacts,
    get_amount_callback,
    get_amounts,
    get_contacts,
    me,
    remove_contacts,
    set_phone,
    start,
    transfer_money,
)
from app.internal.users.db.models import User


@pytest.mark.integration
@pytest.mark.django_db
def test_start_handler(update_mock: Update, context_mock: CallbackContext):
    start(update_mock, context_mock)
    assert User.objects.filter(username=update_mock.effective_chat.username).first()
    update_mock.message.reply_text.assert_called_once()
    assert update_mock.message.reply_text.call_args.args[0]


@pytest.mark.integration
@pytest.mark.django_db
def test_set_phone(update_mock_set_phone: Update, set_phone_context_mock: CallbackContext):
    set_phone(update_mock_set_phone, set_phone_context_mock)
    user = User.objects.filter(username=update_mock_set_phone.effective_chat.username).first()
    assert user
    assert user.phone_number
    assert update_mock_set_phone.message.reply_text.called_with(ANY)


@pytest.mark.integration
@pytest.mark.django_db
def test_me(update_mock_set_phone: Update, set_phone_context_mock: CallbackContext):
    set_phone(update_mock_set_phone, set_phone_context_mock)
    me(update_mock_set_phone, set_phone_context_mock)
    assert update_mock_set_phone.message.reply_text.called_with(ANY)


@pytest.mark.integration
@pytest.mark.django_db
@pytest.mark.parametrize(
    "handler", [me, get_amounts, get_amount_callback, get_contacts, add_contacts, remove_contacts, transfer_money]
)
def test_command_when_no_phone_number(update_mock: Update, context_mock: CallbackContext, handler):
    handler(update_mock, context_mock)
    update_mock.message.reply_text.assert_called_once_with(_phone_number_not_set_error_message)
