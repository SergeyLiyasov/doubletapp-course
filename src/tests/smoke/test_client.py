import pytest
from django.test import Client


@pytest.mark.smoke
def test_site_smoke():
    resp = Client().get("/admin/")
    assert 200 <= resp.status_code < 400
