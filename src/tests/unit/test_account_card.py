from decimal import Decimal

import pytest

from app.internal.accounts.db.models import Account
from app.internal.accounts.domain.services import AccountsService
from app.internal.users.db.models import User


@pytest.mark.unit
@pytest.mark.django_db
@pytest.mark.parametrize(
    "first_account_amount, second_account_amount, sum, new_first_account_amount",
    [
        (0, 0, 0, 0),
        (20, 10, 10, 10),
        (Decimal("2.01"), Decimal("10.3"), Decimal("0.01"), Decimal("2")),
        (20, 10, 20, 0),
        (20, 10, 21, 20),
        (20, 30, 21, 20),
    ],
)
def test_try_transfer(
    test_user, other_test_user, first_account_amount, second_account_amount, sum, new_first_account_amount
):
    new_second_account_amount = first_account_amount + second_account_amount - new_first_account_amount
    first_account = Account.objects.create(id=1, owner=test_user, amount=first_account_amount, number="1")
    second_account = Account.objects.create(id=2, owner=other_test_user, amount=second_account_amount, number="2")
    AccountsService().try_transfer(first_account, second_account, sum)
    assert first_account.amount == new_first_account_amount
    assert second_account.amount == new_second_account_amount
