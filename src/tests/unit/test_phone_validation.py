import pytest

from app.internal.services.phone_validator import validate_phone_number


@pytest.mark.unit
@pytest.mark.parametrize("phone_number", ["+70001112233"])
def test_validate_correct_phone_number(phone_number):
    assert validate_phone_number(phone_number)


@pytest.mark.unit
@pytest.mark.parametrize("phone_number", ["+700011", "++70001112233"])
def test_validate_incorrect_phone_number(phone_number):
    assert not validate_phone_number(phone_number)
