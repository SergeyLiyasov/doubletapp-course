import json

import pytest
from django.test import Client

from app.internal.users.db.models import User
from app.internal.users.domain.services import UsersService

_users_service = UsersService()

# @pytest.mark.unit
# @pytest.mark.django_db
# def test_me_endpoint_when_no_phone_number(test_user: User, test_client: Client):
#     response = test_client.get(f"/api/me/{test_user.telegram_id}")
#     assert response.status_code == 403


# @pytest.mark.unit
# @pytest.mark.django_db
# def test_me_endpoint_when_no_user(test_client: Client):
#     response = test_client.get("/api/me/0")
#     assert response.status_code == 404


# @pytest.mark.unit
# @pytest.mark.django_db
# def test_me_endpoint(test_user: User, test_client: Client):
#     _users_service.set_phone_for_user(test_user, "+70001112233")
#     response = test_client.get(f"/api/me/{test_user.telegram_id}")
#     response_json = json.loads(response.content)
#     assert response.status_code == 200
#     assert response_json["user"]["username"] == test_user.username
#     assert response_json["user"]["first_name"] == test_user.first_name
#     assert response_json["user"]["last_name"] == test_user.last_name
#     assert response_json["user"]["telegram_id"] == str(test_user.telegram_id)
#     assert response_json["user"]["phone_number"] == test_user.phone_number


@pytest.mark.unit
@pytest.mark.django_db
def test_add_to_contacts(test_user: User, other_test_user: User):
    _users_service.add_to_contacts(test_user, [other_test_user.username])
    assert test_user not in other_test_user.contacts.all()
    assert other_test_user in test_user.contacts.all()


@pytest.mark.unit
@pytest.mark.django_db
def test_add_to_contacts_makes_no_duplicates(test_user: User, other_test_user: User):
    _users_service.add_to_contacts(test_user, [other_test_user.username])
    _users_service.add_to_contacts(test_user, [other_test_user.username])
    assert test_user not in other_test_user.contacts.all()
    assert other_test_user in test_user.contacts.all()
    assert test_user.contacts.filter(telegram_id=other_test_user.telegram_id).count() == 1


@pytest.mark.unit
@pytest.mark.django_db
def test_delete_from_contacts(test_user: User, other_test_user: User):
    _users_service.add_to_contacts(test_user, [other_test_user.username])
    _users_service.delete_from_contacts(test_user, [other_test_user.username])
    assert test_user not in other_test_user.contacts.all()
    assert other_test_user not in test_user.contacts.all()


@pytest.mark.unit
@pytest.mark.django_db
def test_delete_from_contacts_not_symmetrical(test_user: User, other_test_user: User):
    _users_service.add_to_contacts(other_test_user, [test_user.username])
    _users_service.delete_from_contacts(test_user, [other_test_user.username])
    assert test_user in other_test_user.contacts.all()
    assert other_test_user not in test_user.contacts.all()


@pytest.mark.unit
@pytest.mark.django_db
def test_delete_from_contacts_when_wasnt_added(test_user: User, other_test_user: User):
    _users_service.delete_from_contacts(test_user, [other_test_user.username])
    assert other_test_user not in test_user.contacts.all()
    assert test_user not in other_test_user.contacts.all()
